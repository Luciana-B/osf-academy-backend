Main = function()
{
	var categoryBtnHandler = function(id){
		$(id).onclick("click", function(){
			goToProducts(id);
		});
	};
	
    var init = function()
    {
      window.history.pushState("", "", '/menu');
	  
    };
    
    var Public =
    {
      init: init,
      myPublicProperty: myPublicProperty,
      myPublicFunction: myPublicFunction
    };
    return Public;
	
  }();