
$("#login").submit(function(event){
	var request;

    event.preventDefault();

    if (request) {
        request.abort();
    }

    var $form = $(this);


    var $inputs = $form.find("input, select, button, textarea");

    var serializedData = $form.serialize();

    $inputs.prop("disabled", true);
	
    request = $.ajax({
        url: "http://localhost/logging",
        type: "post",
        data: serializedData
    });
    
    request.done(function (response, textStatus, jqXHR){
       
		
		var res = document.getElementById("login_response");
		
		
		if(response.status == "success"){
			window.location.replace(response.message);
		}else{
			res.className = 'alert alert-'+response.status;
			res.innerHTML = "<strong>" + response.message + "</strong>";
		}
		
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });


    request.always(function () {
        
        $inputs.prop("disabled", false);
    });

});

