
// Bind to the submit event of our form
$("#register").submit(function(event){
	var request;

    event.preventDefault();

    if (request) {
        request.abort();
    }

    var $form = $(this);

    var $inputs = $form.find("input, select, button, textarea");

    var serializedData = $form.serialize();

    $inputs.prop("disabled", true);

    request = $.ajax({
        url: "http://localhost/register",
        type: "post",
        data: serializedData
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        //console.log("Hooray, it worked!");
		
		var res = document.getElementById("reg_response");
		if (response.status === "success"){
			res.className = 'alert alert-' + response.status;
			res.innerHTML = "<strong>" + response.message + "</strong></br><a class='btn btn-success' href='"+response.ref+"'><strong>Go Back on shop</strong></a>";
		}
		
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });


    request.always(function () {
        
        $inputs.prop("disabled", false);
    });

});
