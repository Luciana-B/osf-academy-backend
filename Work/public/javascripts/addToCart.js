$("#addToCart").submit(function(event){


	var request;

    event.preventDefault();

    if (request) {
        request.abort();
    }

    var $form = $(this);


    var $inputs = $form.find("input, select, button, textarea");

    var serializedData = $form.serialize();
	
    $inputs.prop("disabled", true);
	
	
    request = $.ajax({
        url: "http://localhost/addToCart",
        type: "POST",
		data: serializedData,
    });

    
    request.done(function (response, textStatus, jqXHR){
		
		 document.getElementById("buy").className = 'alert alert-success'
		 document.getElementById("buy").innerHTML = "<strong>Added to shopping cart!</strong>";
		
		
		
		
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
		
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });


    request.always(function () {
        
        $inputs.prop("disabled", false);
    });

});

