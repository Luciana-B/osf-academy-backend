// global variable which contains the items
var it = {};
//the currencies
var c = [];
var getCurrencies;
gapi.load('auth2',function(){
			gapi.auth2.init();
		});


function btn(id){
	return "<div><button class= 'btn btn-danger mb-3' onclick= goToProducts(this) id = '" + id + "'>View Products</button></div>";
};


function goToCategory(el){ 
                
			    var category = el.innerHTML;
				
				var category_buttons = document.getElementsByClassName('btn btn-danger mt-2 mr-2 mb-3');
				for (var i = 0; i < category_buttons.length; i++) {
					category_buttons[i].className = "btn btn-outline-danger mt-2 mr-2 mb-3";
				}
				el.className = "btn btn-danger mt-2 mr-2 mb-3";
				
				document.getElementById("Breadcrumbs").innerHTML = "<nav class='breadcrumb'><a class='breadcrumb-item' ng-click = 'my = !my' href='#'> " + folder +"</a> <a id='bc-home' class='breadcrumb-item' href='#'>" + category + " </a></nav>";
				
				
				document.getElementById("Main_Category").innerHTML = category;
				document.getElementById("Description").innerHTML = "<p>" + it[category].description + "</p>";
				
				html= "<div class='text-danger'>Subcategories</div><hr>";
				for(var i=0; i< it[category].subCategories.length; i++){
					html += "<div><div class='text-danger'>" + it[category].subCategories[i].name + "</div>";
					if(it[category].subCategories[i].image){
						html += "<div><img src='/images/" + it[category].subCategories[i].image + "'></img></div>"; 	
					}
					html += "<div><p>" + it[category].subCategories[i].description + "</p></div>";
				    html += btn( it[category].id + "/" + it[category].subCategories[i].id) + "</div>";
					
				}
				
				
				document.getElementById("Subcategories").innerHTML = html ;
				
};

function categoryBtnHandler(id){
	$(id).onclick("click", function(){
		goToProducts(id);
	});
};

function goToProducts(subCategory){
	var url = window.location.href;
	
	if (url[url.length-1] == "#"){
		url = url.slice(0, -1);
	}
	if(url[url.length-1] === "/"){
		url +=  subCategory.id;
	}else{
		url += "/" + subCategory.id;
	}
	window.location.replace(url);
}
function goToPDP(product){
	
	var url = window.location.href;
	if(url[url.length-1] === "/"){
		url +=  subCategory.id;
	}else{
		url += "/" + product.id;
	}
	window.location.replace(url);
};

function goToMenu(categoryBtn){
	url = window.location.href ;
	if(url.indexOf("menu") > 0){
		url = url.substring(0,url.indexOf("menu")+5) + categoryBtn.id;
	}else{
		url = "http://localhost/menu/" + categoryBtn.id;
	}
	
	window.location.replace(url);
};

function completeBreadcrumb(id, content){
	var el = document.getElementById(id).innerHTML = content.toString();

}
function completeHref(c1, id, c2){
	
	document.getElementById(id).href += c1 + "/" + c2;
	
};

function getCurrencies(){
        //alert(" de ce? "); 
	 	$.ajax({
		type: "GET",
		url: "http://localhost/currencies",
		dataType: "json"
	}).done(function (res) {
		if(res.length <1){
			//alert("Ooop!..Something went wrong!\n You can not change currencies!\n");
			get();
		}
		c = res;
		c.push({IDMoneda:'RON', Value:1.00});
		html = ""; //"<a class='dropdown-item' onclick='changePrice(this)' id='RON'>RON</a>";
		for(var i=0; i<c.length; i++){
			//alert(res[i]);
			html += "<a class='dropdown-item' onclick='changePrice(this)' id='"+res[i].IDMoneda+"'>" + c[i].IDMoneda + "</a>";
		}
		document.getElementById("dd-content").innerHTML = html;
		
		
	}).fail(function (jqXHR, textStatus, errorThrown) {
		//alert("AJAX call failed: " + textStatus + ", " + errorThrown);
		//alert("Ooop!..Something went wrong!\n You can not change currencies!\n");
		//this.getCurrencies();
		
	});
 };
 
 function changePrice(a){
	 var value=0, res=0;
	 var c1, c2, price_elem, price;
	 dd_head = document.getElementById("dd-head");
	 c1 = dd_head.innerHTML;
	 c2 = a.id;
	 price_elem = document.getElementById("price");
	 price = Number(price_elem.innerHTML);
	// var value_c2 = lodash.filter(c, { 'IDMoneda': c2 } ); 
	 
	
	if(c2 === "RON"){
		res = toRON(c1,price);
	}else
	if(c1 === "RON"){
		res = toAnother(c2,price);
	}else{
		var rons = toRON(c1,price);
		res = toAnother(c2,rons);
	};
	 
	 document.getElementById("displayed_price").innerHTML = res.toFixed(2);
	 price_elem.innerHTML = res;
	 dd_head.innerHTML = c2;
	 
};

function toRON(c1, price){
	var value=0;
	for(var i=0;i<c.length;i++){
		 if(c[i].IDMoneda == c1){
			 value = Number(c[i].Value);
			 
		 }
	 }
	return price*value;
};

 function toAnother(c2, price){
	 var value=0;
	 for(var i=0;i<c.length;i++){
		 if(c[i].IDMoneda == c2){
			 value = Number(c[i].Value);
			 
		 }
	}
	return price *(1/value);
 };
 

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
});

};


