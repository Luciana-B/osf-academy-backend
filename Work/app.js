// Module dependencies.
var express = require("express")
  , http    = require("http")
  , path    = require("path")
  , routes  = require("./routes")
  , bodyParser = require('body-parser')
  , MongoClient = require('mongodb').MongoClient
  , assert = require('assert')
  , session = require('express-session');

  
var app     = express();

// All environments
app.set("port", 80);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());

app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true,
	/*cookie: {
        expires: 600000
    }*/
}));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// App routes

app.post("/addToCart", routes.addToCart);
app.post("/logging", routes.logging);
app.post("/register", routes.register);
app.post("/delete_prod", routes.delete_prod);
app.post("/update_nr_prod", routes.update_nr_prod)
app.post("/google", routes.google)
app.get('/logout', routes.logout);
app.get('/cart', routes.cart);



app.get("/"     , routes.index);
app.get("/login", routes.login);
app.get("/register-confirmation=:code", routes.confirmation);
app.get("/hello", routes.hello);
app.get("/menu", routes.menu);
app.get("/main", routes.menu);
app.get("/currencies", routes.currencies);
app.get("/menu/:category", routes.control, routes.menu);
app.get("/menu/:category/:subCategory", routes.products);
app.get("/menu/:category/:subCategory/:product", routes.PDP);




// Run server
http.createServer(app).listen(app.get("port"), function() {
	console.log("Express server listening on port " + app.get("port"));
	
});
