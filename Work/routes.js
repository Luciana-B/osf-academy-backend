var mongodb;
const crypto = require('crypto');
var _         = require("underscore");
var MongoClient = require('mongodb').MongoClient;  

var soap = require('soap');
var url = 'http://infovalutar.ro/curs.asmx?wsdl';

var nodemailer = require('nodemailer');
var randomstring = require("randomstring");
var transporter = nodemailer.createTransport({
	  service: 'Gmail',
	  auth: {
		user: 'osf.online.shop@gmail.com',
		pass: 'myPassword'
	  }
});
var host = 'http://localhost';

// Create the db connection
MongoClient.connect("mongodb://localhost:27017/shop", function(err, db) {  
	    var assert = require('assert');
		assert.equal(null, err);
		mongodb = db;
    }
);

exports.index = function(req, res) {
	res.render("index", { 
		// Template data
		title: "Express",
	});
};

exports.control = function(req, res, next){
	
	req.params.currentCategory = req.params.category;
	
	next();
};

exports.menu = function(req, res) {
	var category;
	//console.log("The category: " + req.params.currentCategory);
	
	if(req.currentCategory, res){
		category = req.params.currentCategory;
	}else{
		category = "";
	}
		var collection = mongodb.collection('categories');
		
		collection.find({},{id:1, name:1,page_description:1, 'categories':1 ,_id:0}).toArray(function(err, items) {
			
			res.render("menu", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				categories : items,
				currentCategory: category,
				se : req.session
				
			});

		});
	  
};

exports.hello = function(req, res) {
	   
		var collection = mongodb.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("hello", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Hello World!",
				items : items
			});

			
		});

};

exports.products = function(req, res){
		
		var subCategory = req.params.subCategory;
		var filter = {primary_category_id : subCategory, 'image_groups.view_type': "medium"};
		var required_info = {id:1, name: 1, page_description:1, price: 1, "image_groups.$":1, '_id':0};
			
		mongodb.collection('categories').find({}, {id:1,name:1,'categories.categories.name':1,'categories.categories.id':1,'_id':0}).toArray(function(err, categories){
			mongodb.collection('products').find(filter, required_info).toArray(function(err, items) {
			
			//res.json(items);
			res.render("subcategory", {
                _     : _, 				
				products: items,
				categories: categories,
				currentCategory: req.params.category, 
				currentSubCategory: subCategory,
				se : req.session
				
				});

			});
			
		});
	
};

exports.PDP = function(req, res){
		console.log(req.session);
	    var product_id = req.params.product;
		var filter = { id : product_id }; 
		var required_info = {name: 1, short_description:1, long_description:1, price: 1, "image_groups.images":1, currency:1,id:1, '_id':0};
			
		mongodb.collection('categories').find({}, {id:1,name:1,'categories.categories.name':1,'categories.categories.id':1,'_id':0}).toArray(function(err, categories){
			mongodb.collection('products').find(filter, required_info).toArray(function(err, items) {
			//res.json(items);
			res.render("pdp", {
                _     : _, 				
				products: items,
				categories: categories,
				currentCategory: req.params.category, 
				currentSubCategory: req.params.subCategory,
				se : req.session
				
				});

			});
			
		});
};

exports.currencies = function(req, response){
	  soap.createClient(url, function(err, client) {
	      var args = {};
		  //var d = res.lastdateinsertedResult.toISOString().slice(0,10).toString();
		  
		  client.lastdateinserted( function(err, res) {
			  
			  if(!err){
				  args={dt:res.lastdateinsertedResult.toISOString().slice(0.10)};
				  client.getall(args, function(error, result) {
				  //console.log(args);
					  if(!error){
						 var c = result.getallResult.diffgram.DocumentElement.Currency;
						 //var v = result.getallResult.diffgram.DocumentElement.Currency.Value  .IDMoneda;
						  //console.log(c);
						  response.json(c);
					  }else{
						  console.log("Eroare!\n" );
					  }
					  
				  });	
			  }else{
				  console.log("Eroare!\n");
			  }
			  
		  });
	
  });
};

exports.login = function(req, res){
	var ref = req.headers.referer;
	console.log(ref);
	res.render("login",{ref: ref});
};


exports.logging = function(req, res){
	if(req.session && req.session.email){
		res.json({status:"warning", message: "Already logged in"});
		

		return; 
	}
	//console.log("Request : " + req.body.log_email + req.body.log_email);
	var email = req.body.email,
		ref = req.body.ref,
	    pass = crypto.createHmac('sha512', req.body.password).digest('hex');
		
	mongodb.collection('users').findOne({email:email},{ _id:0}, function(err, result){
		if(!err){
			console.log("Result " + result);
			if(result){
				if(result.password === pass){
					console.log("success login");
					req.session.firstname = result.firstname;
					req.session.lastname = result.lastname;
					req.session.email = result.email;
					req.session.image = result.image;
					console.log(req.session);
					console.log("[login]");
					res.json({status:"success",message: ref});
					return;
				}else{
					console.log("incorecct email /pass");
					res.json({status:"danger", message:"The email or password you entered is incorrect!"});
				
				return;
				}
			}else{
				console.log("incorecct email /pass");
				res.json({status:"danger", message:"The email or password you entered is incorrect!"});
				
				return;
			}
			
		}else{
			res.json({status:"warning",message:"Please try again!"});
			
			return;
		}
	});

};

exports.register = function(req, res){
	var email = req.body.email,
	    pass = crypto.createHmac('sha512', req.body.password).digest('hex'),
		code = randomstring.generate(),
		first = req.body.firstname,
		last = req.body.lastname,
		ref = req.body.ref,
		image = "https://i.pinimg.com/originals/30/f4/2c/30f42c25ac743c1e643522013bfb14d2.png",
	    user = {firstname: first ,
         		lastname: last, 
				email : email,
				image: image,
				adresses : [],
				password : pass,
				active :  false,
				confirmation : code,
				cart: {
					nr: 0,
					status: "pandding",
					products : []
				}
				},
		url = host + '/register-confirmation=' + code,
	    mailOptions = {
			  from: 'Online Shop <osf.online.shop>',
			  to: email,
			  subject: 'Confirm your registration',
			  html: "<div><h1>Online Shop</h1></div><hr><br><div><h2>Dear "+first+",</h2></div><br><div><h2>Please confima your registration by clicking on the link bellow : </h2></div><div><h2><a href='"+url+"'>"+url+"</a></h2></div><br><br><div><h3>Thank for your registration!</h3></div>"
		},
	    users = mongodb.collection('users'),
		db_res = users.insert(user);
		
		req.session.firstname = first;
		req.session.lastname = last;
		req.session.email = email;
		req.session.image = image;
		
		if(db_res){
			transporter.sendMail(mailOptions, function(error, info){
			  if (!error) {
				console.log(error);
			  } else {
				console.log('Email sent to ' + email + "! from ");
				res.json({status:'success',message:'Please check your email in order to confirm your registration!\n', ref: ref});
			  }
				
			});
			
		}
		
		
};

exports.confirmation =  function(req,res){
	var code = req.params.code;
	
	mongodb.collection('users').findOne({confirmation: code},{_id:0}, function(err, result){
		if(!err){
			if(result){
				res.end("Succesful registered! " + result.firstname);
				
			}else{
				res.end("User not found");
			}
		}else{
			res.end();
		}
	});
	
};

exports.logout = function (req, res) {
  var ref  = req.headers.referer;
  console.log("Logout from " + ref);
  req.session.destroy();
  res.redirect(ref);
};

exports.addToCart = function(req, res){
	var id = req.body.id,
		email = req.session.email,
		name = req.body.name,
		image = req.body.image,
		link = req.headers.referer,
		price = req.body.price,
		product = {id: id, image: image, link:link, nr:1, price:price, name : name};
	console.log("DA - " + req.session.email + link);
	mongodb.collection('users').findOne({email: email, 'cart.products.id': id}, function(err, result){
		if(!err){
			if(result){
				mongodb.collection('users').update({'email': email, 'cart.products.id': id}, {$inc: { 'cart.products.$.nr' : 1 }}, function(err, result){
					res.json({m:'Found it!'});
				});
				
			}else{
				mongodb.collection('users').update({'email': email}, {$push: { 'cart.products' : product }}, function(err, result){
					res.json({m:"Didn't Found it!"});
				});
			}
		}
		
	});
	/*mongodb.collection('users').update({'email': req.session.email}, {$push: { 'cart.products' : req.body.id }}, function(err, result){
		res.json({m:'It worked!'});
	});*/
    
};

exports.cart = function (req, res){
	mongodb.collection('users').findOne({email: req.session.email}, {_id:0, 'cart.products' : 1}, function(err, cart){
		mongodb.collection('categories').find({}, {id:1,name:1,'categories.categories.name':1,'categories.categories.id':1,'_id':0}).toArray(function(err, categories){
			
				if(!err){
				res.render("cart", {
					cart : cart.cart,
					 _     : _, 				
					categories: categories,
					se : req.session,
					currentCategory: "", 
				});
			}

			
		});
		
	})
};

exports.delete_prod = function(req, res){
	var id = req.body.id,
		email = req.session.email,
		url = req.headers.referer;
	mongodb.collection('users').update({'email': email}, {$pull: { 'cart.products' : { 'id' :  id}}}, function(err, result){
		if(!err){
			res.redirect(url);
		}
	});
	
};

exports.update_nr_prod = function(req, res){
	
	var id = req.body.id,
		email = req.session.email,
		url = req.headers.referer,
		nr = req.body.nr;
	console.log("Update " + id + " " +nr);
	mongodb.collection('users').update({'email': email, 'cart.products.id': id}, {$set: { 'cart.products.$.nr' : nr }}, function(err, result){
		if(!err){
			res.redirect(url);

		}
	});
};

exports.google = function(req, res){
	//var url = req.session.ref;
	console.log(req.body.name + " " +req.body.email  + " " + req.body.image + " " + url);
	req.session.firstname = req.body.name;
	req.session.lastname = "";
	req.session.email = req.body.email;
	req.session.image = req.body.image;
	
	res.end(url);
	
};